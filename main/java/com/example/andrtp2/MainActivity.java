package com.example.andrtp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {
    ListView list;
    BookDbHelper dbb;
    SimpleCursorAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbb =new BookDbHelper(getApplicationContext());
        //dbb.populate();
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        Cursor result =dbb.fetchAllBooks();
        // Declaration of an adapter with items with 2 elements
        adapter = new SimpleCursorAdapter(MainActivity.this, android.R.layout.simple_list_item_2,
                result, new String[] { BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS },
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);
        // Associate the adapter with the list view
        list = (ListView) findViewById(R.id.listview);
        registerForContextMenu(list);
        adapter.changeCursor(dbb.fetchAllBooks());
        adapter.notifyDataSetChanged();
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent myIntent = new Intent(view.getContext(), BookActivity.class);
                Cursor item = (Cursor)list.getItemAtPosition(position);
                Book book=dbb.cursorToBook(item);
                myIntent.putExtra("book", book);
                startActivityForResult(myIntent, 0);
            }
        });



        fab.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {

                Intent myIntent = new Intent(view.getContext(), BookActivity.class);
                startActivityForResult(myIntent, 0);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show(); }
        }); }
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present. getMenuInflater().inflate(R.menu.menu_main, menu);

        return true; }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        // TODO: Implement this method
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);


    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) { return true; }
        return super.onOptionsItemSelected(item);
    }
    @Override

    public boolean   onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.supprimer: Cursor iteme = (Cursor)list.getItemAtPosition(info.position);

                dbb.deleteBook(iteme);
              adapter.changeCursor(dbb.fetchAllBooks());
                adapter.notifyDataSetChanged();
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.changeCursor(dbb.fetchAllBooks());
        adapter.notifyDataSetChanged();

    }
}




