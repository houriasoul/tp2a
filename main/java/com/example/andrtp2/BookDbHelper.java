package com.example.andrtp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {


    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    static final String CREATE_TABLE_LIBRARY = "CREATE TABLE "
            + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_BOOK_TITLE
            + " TEXT, " + COLUMN_AUTHORS + " TEXT, " + COLUMN_YEAR
            + " TEXT, " +COLUMN_GENRES + " TEXT, "+COLUMN_PUBLISHER + " TEXT )";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL(CREATE_TABLE_LIBRARY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Log the version upgrade.
        Log.w("TaskDBAdapter", "Upgrading from version " +oldVersion + " to " +newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + "TEMPLATE");
        // Create a new one.

        onCreate(db);
    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues v = new ContentValues();

        v.put(COLUMN_BOOK_TITLE, book.getTitle());
        v.put(COLUMN_AUTHORS, book.getAuthors());
        v.put(COLUMN_GENRES, book.getGenres());
        v.put(COLUMN_PUBLISHER, book.getPublisher());
        v.put(COLUMN_YEAR, book.getYear());

        // Inserting Row
        long rowID = 0;

        // call db.insert()
        rowID = db.insert(TABLE_NAME, null, v);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res;

        // updating row
        // call db.update()
        ContentValues newvalue = new ContentValues();
        newvalue.put(COLUMN_BOOK_TITLE, book.getTitle());
        newvalue.put(COLUMN_AUTHORS, book.getAuthors());
        newvalue.put(COLUMN_GENRES, book.getGenres());
        newvalue.put(COLUMN_PUBLISHER, book.getPublisher());
        newvalue.put(COLUMN_YEAR, book.getYear());
        res= db.update(TABLE_NAME, newvalue, _ID + " = ?",
                new String[] { String.valueOf(book.getId()) });

        return res;

    }


    public Cursor fetchAllBooks() {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM library  " ;
        Cursor cursor;

        // call db.query()

        cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        //String id=String.valueOf(ID);
        Book book = cursorToBook(cursor);


        db.delete(TABLE_NAME, _ID + " = ?",
                new String[] { String.valueOf(book.getId()) });


        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
        // build a Book object from cursor

        book = new Book();
        book.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        book.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_TITLE)));
        book.setAuthors(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHORS)));
        book.setGenres(cursor.getString(cursor.getColumnIndex(COLUMN_GENRES)));
        book.setPublisher(cursor.getString(cursor.getColumnIndex(COLUMN_PUBLISHER)));
        book.setYear(cursor.getString(cursor.getColumnIndex(COLUMN_YEAR)));

        return book;
    }
}