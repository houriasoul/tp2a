package com.example.andrtp2;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {
    EditText nmbook;
    EditText author;
    EditText annee;
    EditText genre;
    EditText publ;
    Button sauvg;
    BookDbHelper dbb;
    Book book;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        dbb= new BookDbHelper(getApplicationContext());
        nmbook=(EditText)findViewById(R.id.nameBook);
        author=(EditText)findViewById(R.id.editAuthors);
        annee=(EditText)findViewById(R.id.editYear);
        genre=(EditText)findViewById(R.id.editGenres);
        publ=(EditText)findViewById(R.id.editPublisher);
        sauvg=(Button)findViewById(R.id.button) ;
        Bundle extras = getIntent().getExtras();



        if(extras!=null){{
            if(extras.containsKey("book"))


                book =(Book) extras.get("book");

            if(book!=null){
                nmbook.setText(book.getTitle());
                author.setText(book.getAuthors());
                annee.setText(book.getYear());
                genre.setText(book.getGenres());
                publ.setText(book.getPublisher());
                sauvg.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        book.setTitle(nmbook.getText().toString());
                        book.setAuthors(author.getText().toString());
                        book.setYear(annee.getText().toString());
                        book.setGenres(genre.getText().toString());
                        book.setPublisher(publ.getText().toString());
                        dbb.updateBook(book);
                        Intent myIntent = new Intent(BookActivity.this, MainActivity.class);

                        //startActivityForResult(myIntent, 0);
                        //affichage de sauvgarde
                        Toast.makeText(v.getContext(),"sauvgarded ",Toast.LENGTH_LONG).show();
                    }
                });} }
        }else {
            sauvg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // TODO Auto-generated method stub
                    book= new Book();


                    book.setTitle(nmbook.getText().toString());
                    book.setAuthors(author.getText().toString());
                    book.setYear(annee.getText().toString());
                    book.setGenres(genre.getText().toString());
                    book.setPublisher(publ.getText().toString());
                    if(nmbook.getText().toString().equals("")){
                        AlertDialog alertDialog = new AlertDialog.Builder(BookActivity.this).create();
                        alertDialog.setTitle("attention");
                        alertDialog.setMessage("the name can not be empty");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    else{
                        dbb.addBook(book);
                        Intent myIntent = new Intent(BookActivity.this, MainActivity.class);

                       // finish();
                     //   startActivityForResult(myIntent, 0);
                        Toast.makeText(v.getContext(),"sauvgarded ",Toast.LENGTH_LONG).show();}
                }
            });

        }


    }
}

